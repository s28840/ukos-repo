def add(a, b):
    return a + b


def main():
    print('''
  ___ _         _              __         _             _    _
 | _ ) |__ _ __(_)_ _  __ _   / _|__ _ __| |_   __ _ __| |__| |___ _ _
 | _ \\ / _` |_ / | ' \\/ _` | |  _/ _` (_-<  _| / _` / _` / _` / -_) '_|
 |___/_\\__,_/__|_|_||_\\__, | |_| \\__,_/__/\\__| \\__,_\\__,_\\__,_\\___|_|
                      |___/
    ''')

    x = input('First number: ')
    y = input('Second number: ')

    x = float(x)
    y = float(y)

    print(f'{x} + {y} = {add(x, y)}')


if __name__ == '__main__':
    main()
