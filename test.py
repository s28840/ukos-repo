from random import randrange
from sys import exit
from src.main import add

NUM_TESTS = 50
MAX_NUM = 10_000


def test_add():
    print('Testing add function...')

    def ident(a):
        return add(a, 0) == a

    def comm(a, b):
        return add(a, b) == add(b, a)

    def assoc(a, b, c):
        return add(add(a, b), c) == add(a, add(b, c))

    TESTS = [
        (1, 'Identity', ident),
        (2, 'Commutative', comm),
        (3, 'Associative', assoc),
    ]

    failed = 0
    for test in TESTS:
        args, name, fun = test
        passed = True
        print(f'\tChecking {name}... ', end='')
        for _ in range(NUM_TESTS):
            fun_args = [randrange(MAX_NUM) for _ in range(args)]
            result = fun(*fun_args)
            if not result:
                # nonlocal passed
                passed = False
                break
        if passed:
            print('Passed!')
        else:
            print('Failed')
            failed += 1
    tests_len = len(TESTS)
    passed = tests_len - failed
    print(f'\nAll {tests_len} tests checked: {passed} passed, {failed} failed')
    if failed > 0:
        exit(1)


if __name__ == '__main__':
    test_add()
